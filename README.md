Recommender System for phys.org is created by Vladislav Lazarev. 

The goal was to build console tool that:

1) Find and download articles from phys.org from the given period (date_from - date_to).

2) Can evaluate articles as "I like it" or "I dislike it" (-1 or 1)

3) Suggest new articles, basing on estimated pages

In general the programm includes crawler, that find and download articles, storing them in the mongoDB. Suggesting is based 
on machine learning linear classificator.

In the source you can find Makefile, which can help you to compile each file. You need python 3, mongoDB and 
some specific python libraries, which you can see in the "import" sections.

List of files:

1) log.py - logging properties

2) common.py - list of the functions and constants.

3) erase.py - deleting database

4) evaluate.py - evaluation of articles

5) news_parser.py - parsing of an article

6) archive_parser.py - parsing and extracting the set of articles from archive pages

7) update.py - search and store articles from the given date period.

8) clean.py - parse articles and database

9) get_articles.py - find articles with best score


You can find Makefiles for update.py, clean.py, erase.py and get_articles.py.