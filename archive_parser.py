#!/usr/local/bin/python3
# -*- coding: utf-8 -*-


from html.parser import HTMLParser
import urllib.request
import config
import re


def is_phys_article(url):
    pattern = 'http://phys\.org/news/\d{4}\-\d{2}\-[a-z]+(\-[a-z]+)*\.html'
    return re.match(pattern, url);


class ArchiveParser(HTMLParser):

    def __init__(self):
        HTMLParser.__init__(self)
        self._url_tag_depth = 0
        self._urls = set()


    def handle_starttag(self, tag, attrs):
        if tag == 'h3' and self._url_tag_depth == 0:
            self._url_tag_depth += 1
        if tag == 'a' and self._url_tag_depth == 1:
            self._url_tag_depth += 1
            attrs = dict(attrs)
            if 'href' in attrs:
                url = attrs['href'];
                if is_phys_article(url):
                    self._urls.add(url);


    def handle_endtag(self, tag):
        if self._url_tag_depth == 2 and tag == 'a':
            self._url_tag_depth -= 1
        if self._url_tag_depth == 1 and tag == 'h3':
            self._url_tag_depth -= 1


    def parse(self, page):
        self.feed(page)
        return self._urls



def parse(page):
    return ArchiveParser().parse(page)
