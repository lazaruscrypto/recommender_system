#!/usr/local/bin/python3
# -*- coding: utf-8 -*-


import collections
import sys
import datetime
import log
import argparse
import config
import re
import pymongo
import news_parser
import common



def clean_parser():
    parser = argparse.ArgumentParser(
        usage = 'clean phys-crawler [-h] [--log] [--from] [--to]',
        description = 'Phys-crawler page cleaner'
    )
    parser.add_argument(
        '--log',
        default = 'error',
        help = 'log level',
        choices = ['critical', 'error', 'debug'],
        dest = 'log_level'
    )
    return parser.parse_args()


def main():
    args = clean_parser()
    log.config(log.level(args.log_level))
    try:
        log.debug('Starting phys-crawler for cleaning')
        client = pymongo.MongoClient()
        articles = client.articles.collection
        parse_urls = set()
        for article in articles.find():
            if not len(article['topic']):
                parse_urls.add(article['url'])
        for url in parse_urls:
            log.debug('Parse %s', url)
            page = articles.find_one({'url' : url})['html']
            text, title, topic = news_parser.parse(page)
            if topic == '':
                topic = 'Random'
            articles.find_one_and_update({'url' : url}, {'$set' : {'topic' : topic, 'title' : title, 'text' : text}})
    except KeyboardInterrupt:
        log.debug('Force phys-crawler to stop...')
        return
    except Exception:
        _, value, trace = sys.exc_info()
        log.critical('%s', trace)
   

if __name__ == '__main__':
    main()