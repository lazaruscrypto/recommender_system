#!/usr/local/bin/python3
# -*- coding: utf-8 -*-


import config
import datetime
import log
import os
import time
import os.path
import urllib.request



def extractDate(url):
    preffix_len = len(config.PHYS_ARCHIVE)
    return strToDate(url[preffix_len: preffix_len + 10])


def dateToUrl(date, page_n):
    return config.PHYS_ARCHIVE + dateToStr(date) + '/page' + str(page_n) + '.html'


def dateToStr(date):
    return date.strftime('%d-%m-%Y')


def strToDate(date_str):
    return datetime.datetime.strptime(date_str, '%d-%m-%Y')


def loadPage(url):
	time.sleep(1.0)
	try:
	    response = urllib.request.urlopen(url)
	    page = str(response.read(), encoding='utf-8')
	    log.debug('Load "%s"... %s %s', url, response.reason, response.getcode())
	    return page
	except (urllib.error.URLError, urllib.error.HTTPError) as exception:
		log.error('Load "%s"... %s %s', url, exception.reason, exception.code)
		return None
    


