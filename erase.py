#!/usr/local/bin/python3
# -*- coding: utf-8 -*-


import collections
import log
import pymongo

def drop(db):
	collection = db.collection
	db.drop_collection(collection)


def main():
    client = pymongo.MongoClient()
    db = client.articles
    drop(db)
    db = client.words
    drop(db)
    log.debug('Clear the mongoDB')


if __name__ == '__main__':
    main()