#!/usr/local/bin/python3
# -*- coding: utf-8 -*-


import collections
import sys
import argparse
import log
import config
import common
import pymongo


def evaluate_parser(argv):
    parser = argparse.ArgumentParser(
        usage = 'evaluate [-h] [urls]',
        description = 'Like the set of pages'
    )
    parser.add_argument(
        '--log',
        default = 'error',
        help = 'log level',
        choices = ['critical', 'error',  'debug'],
        dest = 'log_level'
    )
    parser.add_argument(
        'urls',
        nargs = '*',
        help = 'urls to evaluate'
    )
    parser.add_argument(
        '--mark',
        type = int,
        default = 1,
        help = 'mark for articles in the set {-1, 1}',
        dest = 'mark'
    )
    return parser.parse_args()


def main():
    args = evaluate_parser(sys.argv)
    log.config(log.level(args.log_level))
    client = pymongo.MongoClient()
    articles = client.articles.collection
    for url in args.urls:
        if not articles.find_one({'url' : url}):
            continue
        articles.find_one_and_update({'url' : url}, {'$set' : {'mark' : args.mark}})
        log.debug('Set mark to %s as %d', url, args.mark)
    

if __name__ == '__main__':
    main()