#!/usr/local/bin/python3
# -*- coding: utf-8 -*-


import collections
import sys
import numpy
import argparse
import re
import log
import config
import common
import pymongo
import sklearn.feature_extraction.text
import sklearn.linear_model
import sklearn.metrics
import sklearn.grid_search
import scipy
import random


def learn_parser(argv):
    parser = argparse.ArgumentParser(
        usage = 'learn [-h]',
        description = 'Machine learning for articles'
    )
    parser.add_argument(
        '--log',
        default = 'error',
        help = 'log level',
        choices = ['critical', 'error',  'debug'],
        dest = 'log_level'
    )
    parser.add_argument(
        '--articles',
        type = int,
        default = 1,
        help = 'best articles to show',
        dest = 'articles'
    )
    return parser.parse_args()



def scorer(estimator, X, Y):
    return sklearn.metrics.roc_auc_score(Y, estimator.predict_proba(X)[:, 1])


def main():
    args = learn_parser(sys.argv)
    log.config(log.level(args.log_level))
    client = pymongo.MongoClient()
    articles = client.articles.collection
    vectorizer = sklearn.feature_extraction.text.TfidfVectorizer(token_pattern=config.WORD_PATTERN)
    documents = []
    marks = []
    urls = []
    border = 0
    log.debug('Creating train set')
    for article in articles.find():
        if article['mark'] != 0:
            documents.append(article['text'])
            marks.append(article['mark'])
            border += 1
    for article in articles.find():
        if article['mark'] == 0:            
            urls.append(article['url'])
            documents.append(article['text'])
            marks.append(article['mark'])
    X = vectorizer.fit_transform(documents)
    Y = numpy.array(marks)
    X_train, X_get = X[:border], X[border:]
    Y_train, Y_get = Y[:border], Y[border:]
    log.debug('Classifier')
    cls = sklearn.linear_model.SGDClassifier(alpha=0.0001, average=False, class_weight=None, epsilon=0.1,
       eta0=0.0, fit_intercept=True, l1_ratio=0.15,
       learning_rate='optimal', loss='log', n_iter=5, n_jobs=1,
       penalty='l2', power_t=0.5, random_state=None, shuffle=True,
       verbose=0, warm_start=False)
    cls.fit(X_train, Y_train)
    metric = sklearn.metrics.roc_auc_score
    Y_pred = cls.predict_proba(X_get)[:, 1]
    used = numpy.zeros(len(Y_pred), bool)
    count = 0
    log.debug('Collecting the best articles')
    for i in range(1, len(Y_pred)):
        best = -1
        count += 1
        for j in range(1, len(Y_pred)):
            if not used[j] and (best == -1 or Y_pred[j] > Y_pred[best]):
                best = j
        used[best] = 1
        print(urls[best])
        if count >= args.articles:
            break




if __name__ == '__main__':
    main()