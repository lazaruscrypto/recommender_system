#!/usr/local/bin/python3
# -*- coding: utf-8 -*-


from html.parser import HTMLParser


def parse_topic(name):
    return name.strip().replace(' ', '_')


class NewsParser(HTMLParser):

    def __init__(self):
        HTMLParser.__init__(self)
        self._text_tag = False
        self._text = ''
        self._title_tag = False
        self._title = ''
        self._topic_tag = False
        self._topic = ''
        self._count_title = 0
        self._find_text = True


    def handle_starttag(self, tag, attrs):
        attrs = dict(attrs)
        if tag == 'p':
            if self._find_text:
                self._text_tag = True
            if 'class' in attrs and attrs['class'] == 'news-relevant':
                self._find_text = False
        if tag == 'script':
            self._text_tag = False
        if tag == 'footer':
            self._find_text = False
        if tag == 'title':
            self._title_tag = True
        if tag == 'span':
            if 'itemprop' in attrs and attrs['itemprop'] == 'title':
                self._count_title += 1
                if self._count_title == 3:
                    self._topic_tag = True;


    def handle_endtag(self, tag):
        if self._text_tag and tag == 'p':
            self._text_tag = False
        if self._title_tag and tag == 'title':
            self._title_tag = False
        if self._topic_tag and tag == 'span':
            self._topic_tag = False


    def handle_data(self, data):
        if self._text_tag and self._find_text:
            self._text += data + ' '
        if self._title_tag:
            self._title = data
        if self._topic_tag:
            self._topic = parse_topic(data)


    def parse(self, page):
        self.feed(page)
        return (self._text, self._title, self._topic)


def parse(page):
    return NewsParser().parse(page)