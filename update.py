#!/usr/local/bin/python3
# -*- coding: utf-8 -*-


import collections
import os.path
import sys
import argparse
import datetime
import urllib.request
import log
import config
import archive_parser
import common
import pymongo



def update_parser():
    parser = argparse.ArgumentParser(
        usage = 'update phys-crawler [-h] [--log] [--from] [--to]',
        description = 'Phys-crawler page downloader'
    )
    parser.add_argument(
        '--log',
        default = 'error',
        help = 'log level',
        choices = ['critical', 'error', 'debug'],
        dest = 'log_level'
    )
    parser.add_argument(
        '--from',
        type = common.strToDate,
        default = datetime.date(year=2004, month=7, day=1),
        help = 'start of the download interval',
        dest = 'date_from'
    )
    parser.add_argument(
        '--to',
        type = common.strToDate,
        default = datetime.date.today(),
        help = 'end of the donwload interval',
        dest = 'date_to'
    )
    return parser.parse_args()


def extractData(url, articles, date):
    log.debug('Get urls from %s', url)
    page = common.loadPage(url)
    urls = archive_parser.parse(page)
    for url in urls:
        if not articles.find_one({'url' : url}):
            page = common.loadPage(url)
            log.debug('Extract HTML from %s', url)
            articles.insert_one({'url' : url, 'html' : page, 'mark' : 0, 'topic' : ''})


def urls_generator(date_from, date_to):
    date = date_from
    delta = datetime.timedelta(days=1) 
    while date <= date_to:
        for page in range(1, config.DATE_MAX_PAGES):
            url = common.dateToUrl(date, page)
            yield url        
        date += delta


def main():
    args = update_parser()
    log.config(log.level(args.log_level))
    client = pymongo.MongoClient()
    articles = client.articles.collection
    try:
        log.debug('Starting phys-crawler for downloading')
        for url in urls_generator(args.date_from, args.date_to):
            extractData(url, articles, common.extractDate(url))
    except KeyboardInterrupt:
        log.debug('Force phys-crawler to stop...')
        return
    except Exception:
        _, value, trace = sys.exc_info()
        log.critical('%s', trace)
    

if __name__ == '__main__':
    main()